At present, to get a project added to database, please download the blank template, fill out
using a plain-text editor such as Atom.app and send to the editor at editors@fieldwalker.org .
Similarly, to suggest changes to the database or update projects already here, download the
current version of the .yml and/or .bib file and send the updated files to the editor.

We hope to include a more user-friendly system of submission in the future.

## Structure of the database

The database is organised as plain-text YAML formatted files per project. YAML is a human- and machine-readable format that allows hierarchical organisation of data. Project description data files have two main types of information included:

1. *core* schema, for data which describes basic identifying information about the survey, its approximate location, the team leadership, the seasons of work and links to associated project website and open data sources.
2. *extensions*, especially for data which has a more complex hierarchical structure. The most important extension is *survey_methods*, which is a schema designed to describe applied survey counting and sampling strategies.

All project description files are contained in the /projects/ directory, named by project id.

Detailed data may be missing or unavailable for many of the projects recorded here.

## Database fields and explanation

A blank template for a project file is provided as `project-blank.yml`.

### Core project description data

- `id:` a unique ID for the project, usually based on the abbreviation, short-form or acronym given in abbreviation: this will be assigned definitively after submission to database, e.g. `tras`
- `current_at_date:` the date at which this data is current or has been most recently updated, e.g. `2020-04-14`
- `contributor:` the name of the people who have contributed this data description, which may or may not be the same as the project leaders, e.g. `['Luke FIELDWALKER']'`
- `abbreviation:` a short abbreviated version or acronym of the project name, e.g. `TRAS`
- `title:` the full official title of the project, e.g. `"Tatooine Regional Archaeological Survey"`
- `project_leaders:` a list of major leaders or participants, with surnames in capitals, e.g. `['Luke FIELDWALKER', 'Leia FIELDWALKER', 'Obi-Sherd KENOBI']`
- `project_url:` a URL web pointing to the project's public web page(s), e.g. `https://www.survey.newrepublic.com/`
- `opendata_url:` a URL web link pointing to open and publically downloadable digital data from the survey, e.g. `https://www.zenodo.org/tras/03403430`
- `seasons:` a list of years during which fieldwork was undertaken, e.g. `[2003, 2004]`
- `country:` the modern country in which the survey took place, or under whose legal framework the fieldwork was undertaken, as 2-character ISO code, e.g. `TU`
- `latitude:` latitude in decimal degrees for an approximate centre point for the survey (this is to enable mapping of dots on large-scale regional maps), e.g. `33.72388889`
- `longitude:` longitude in decimal degrees for an approximate centre point for the survey (this is to enable mapping of dots on large-scale regional maps) e.g. `10.74972222`
- `map_zoom:` a zoom level for use in web-mapping interfaces such as Leaflet or Google Maps to capture the survey area, e.g. `12`
- `summary:` an extended 100-300 word human-readable description of the project, summarising the aims, novel aspects and research questions of the survey

### Bibliographic references (`references:`)

A list of publications can be provided as a separate BibTex file. These references will be saved under a matching filename in the `/references/` directory. All relevant "\@citekeys" from these files can be associated to the project.

- `references:` can be: `NULL` for no references, `ALL` for all references in associated BibTex file, or a selective YAML-style list of citekeys `[@Skywalker2020,@Skywalkeretal2019]`

### Extensive survey area and values (`survey_**`)

The extensive survey area section provides information about the effective *full* area of the
extensive survey, e.g. the maximum extent of survey permits, as an indication of the area
or region for which claims can be made from the results. In many cases, maximum areas
may be far larger than the area for which specific methods are applied.

- `survey_region:` a (long) WKT string, describing the outline of the maximal permit or extensive survey region, as multipolygon using geographic Lat/Long WGS84 coordinates (CRS=4326). This string can be exported from a shapefile using most GIS programs
- `survey_maximum_area:` the area of the maximum permit or extensive survey region in m2 or ha (please specify), e.g. `80ha`
- `survey_total_no_of_sites:` the total number of sites recorded (new or already known) during the course of the survey up to the `current_at_date` date, e.g. `23`
- `survey_total_estimated_cost:` (optional) the estimated total cost to run the survey across all seasons in EUR/USD/GBP, e.g. `19000EUR`. This should only appear here, if the costs cannot be
broken down by individual methods. Costs appearing in methods will be added to this
cost to create the full accumulated cost.

### Methods description data extension (`survey_methods`)

One of the long-established problems of survey archaeology is comparison of
results between different projects when very different methods are used and
when publication is very varied in its format. Comparisons between survey
methods are often of a vague binary type: e.g. one survey used
"total collection", while another did not; or a rather flexible range
levels of intensity and coverage encapsulated by the description
"intensive survey". Ideally we'd like to be able to ask, "how intensive is
intensive?"... This will become increasingly important as technologically aided
methods become more popular (e.g. drone-assisted survey).

The methods description extension, attempts to encapsulate the specifics of
the methods and method-related results of this project in a structured way, to
allow approximate contextualisation, comparison and characterisation of
methods, preferably in a quantified manner so that comparisons are more
meaningful.

Obviously it may not be possible to fill out all fields, especially for current
projects which have not completed publication, but the more information
included, the more meaningful comparisons can be made.

Methods appear as unnamed list nodes in the yaml built around "units",
under the `survey-methods:` field hence:

```yaml
id: project2345
summary: "An amazing survey project"
## METHODS
survey_methods:
  -   unit_name: tract
      method_description: "A human-readable description of the method in one or two sentences"
      ... further details ...
  -   unit_name: grid
      method_description: "A human-readable description of the method in one or two sentences"
```

For each unit type, the following fields are recognised and can be recorded:

- `unit_name:` a descriptive name used for the methodological unit by the project, e.g. `tract`
- `method_type:` one of the broad method types (see below under *Defining method units and types*), e.g. `transect`
- `method_description:` either a full citation or citekey from the associated bibliography for the project, listing the source publication for the descriptive data about this project's method, or else a note on who provided the data.
- `method_region:` a WKT string describing the total region covered by units of this type, e.g. `"MULTIPOLYGON (((27.2845204782536 37.5361927456389, ...`
- `method_total_area:` the total area covered by all units using this method in m2 or ha (please specify), e.g. `86800m2`
- `total_counted_sherds:` the total of all counted pottery sherds in *all* units of this type
- `max_counted_sherds:` the maximum number of counted ceramic sherds per unit across units of this type
- `mean_counted_sherds:` the mean number of counted ceramic sherds per unit across units of this type
- `min_counted_sherds:` the minimum number of counted ceramic sherds per unit across units of this type
- `mean_counted_othermaterials:` the mean number of counted non-ceramic finds per unit across units of this type
    mean_sampling_collection_percent: [the mean percentage of counted sherds which were collected or examined as diagnostics per unit]
- `sample_criteria:` a brief description of the criteria by which sherds from the total count were sampled for further study
- `total_sampled_sherds:` the total number of ceramic sherds which were sampled for further study from this method, 27000
- `mean_sampled_percent:` the mean of the percentage of sampled sherds to counted sherds, across all units, e.g. `24.4768105%`
- `sd_sampled_percent:` the standard deviation of the percentage of sampled sherds to counted sherds, across all units,e.g. `25.5620884%`
- (optional) `transect_max_length:` the 1-dimensional spatial length of a walker transect, e.g. `100m`
- `unit_area:` the 2-dimensional spatial area of a typical or standard unit in m2, e.g. `10x10m` or `100m2`
- `walkers_per_unit:` the typical number of walkers in unit, e.g. `1`
- `inter_walker_spacing:` for units which include multiple walkers, the standard spacing between walkers in unit, e.g. `5m`, `10m` or `15m`
- `total_sites`: the total number of sites identified or studied using this method (if sites are used as a concept in this project)
- `total_units:` the total number of units of this type walked across all seasons, e.g. `232`
- `total_days:` the total number of days taken to walk all units of this type, across all seasons (follow up analysis, such as subsequent in-depot find study should not be included in field units, and instead recorded as a child method, see below), e.g. `40`
- `total_walkers:` the total number of fieldwalkers who participated in the application this method for all seasons
- `total_estimated_cost:` the estimated total cost to run this method across all seasons in EUR/USD/GBP, e.g. `19000EUR`
- `parent_method:` (see below under *Defining method units relationships (parent and child methods)*)

Where data is not available, `NULL` should be inserted or the field left out completely.

#### Defining method units and types

Since the terminology around methodological units is project specific and not very
standardised, the `unit_name:` should be the name used in the project's own documentation
and then the method should be linked to one of the restricted but broad `method_type:`s.

At present, the options for unit method_types are:

- *`extensive`* - an unsystematic method, usually based visiting sites based on reports from locals, or previous knowledge from
- *`transect`* - a walker line or transect, where a walker is required to walk in a more-or-less straight line, for a certain maximum distance (`transect_max_length:`), within a certain zone (described by `unit_area:`). Transect-style units often have multiple walkers per tract
- *`enclosure`* - a enclosed spatial area, which can either be a regularly shaped grid unit, or an irregular polygonal shape in which walkers must walk in a relatively non-linear manner
- *`subsample`* - (a child method) a subsample of finds from a particular spatial unit, unless otherwise specified, values such as unit_area and method_region are inherited from parent methods.

Many projects actually apply multiple method units, but it is not always obvious that this is
the case. For example, "gridding" may be a cited method, but gridding on 10x10m squares
and "5x5m" squares provides a different level of intensity and hence in this schema should
be treated as two different method units of equal weight. "Unwalkable" tracts are sometimes
recorded as methodological indication of where study was not possible, this is effectively
a different method unit to a "walkable" tract.

Sometimes, method units have parent-child relationships: within "gridding", subsampling
may also be applied, i.e. within a 10x10m grid a 1m2 total collection may be used: this
should be treated as a separate *child* method unit. Tract assemblages may be examined
*in toto* in the field, but then subsampled for collection and further study, which is
effectively another child method unit. Even the standard "tract" methods may sometimes be
better treated as two units: "tracts" and "walker-lines", where walker lines are a child
"transect" method of the parent "enclosure" method, tract, if sufficient data is associated
with both levels of unit.

The choice of what method units to include and how to relate them must be a judgement
call, based on the degree of significance of the method to a project. Sometimes,
methodology is experimental and will not form a significant part of the final results, in
which case these units can be ignored.

#### Defining method units relationships (parent and child methods)

Relationships between methodological units can be indicated through the
`parent_method:` field, using `unit_name:` identifiers. It is possible that a method can be
inherited from two different parent methods if the project method demands it, but this
creates many complexities to describe the project method. It may be better to split these
entities into different versions of the same unit.

This hierarchical relationship between methods may have an effect on how other fields
should be read: for example, total costs should be treated as inherited from child to
parent, so that the true total cost will be accumulated upwards.

### Datable finds per period description data extension (`datable_finds:`)

A further extension to the `survey_methods` can be used to record the aggregate summary
of the finds for the particular method, which can then be compared to different surveys.
This schema has not been tested, yet, but has been designed to be flexible to the
requirements of individual survey projects. It relies on the concept of `periods` which are
defined on a project-basis to reflect regional differences. For each period, the total
number of finds datable to this period can be defined, as can the total number of finds
which _could_ be datable to this period (but could also be dated to other periods):

```yaml
period_name: Modern [a project-defined period]
period_start: 1920 [the approximate start of the period in AD/BC, note that BC dates are written as minus years]
period_end: 2020 [the approximate start of the period]
find_type: pottery sherds
total_dated_finds: 0 [an approximate total number of all datable finds dating to this period]
total_dated_finds_uncertain: 0 [optional adjustment: an approximate total number of all datable finds possibly, but not certainly dating to this period]
```

Different find types can have different periods to reflect the resolution of different categories
of material (e.g. ceramics vs ground stone lithics). The find type can be specified on a
project-appropriate manner. However, the data is most useful when the find_type matches
that of other projects, however, since it means that numbers can be meaningfully
compared: it is recommended that "pottery sherds" is a good standard find type to record,
for all surveys where appropriate.

Recommended standard find_type labels:
- `pottery sherds`
- `chipstone lithics`
- `groundstone lithics`
- `metallurgical objects`
- `architectural fragments`

Periods are listed as sub-fragments of `datable_finds:` under the method fragment, thus:

```yaml
id: project2345
summary: "An amazing survey project"
## METHODS
survey_methods:
  -   unit_name: tract
      method_description: "A human-readable description of the method in one or two sentences"
      ...
      datable_finds:
        -   period_name: Bronze Age
            period_start: -3200
            period_end: -1200
            total_dated_finds: 112
            total_dated_finds_uncertain: 344
        -   period_name: Iron Age
            period_start: -1200
            ...
```

Where subsampling from methods is done, it may be best to record the datable_finds
within the child subsample method rather than the parent collection method.

### Meta-description of data (`meta_notes:`)

Notes about the quality and content of the data about the project can be included in the
`meta_notes:` field as long string. Structured data should not be included and this field
should really only be used as memo for those editing the database directly.
