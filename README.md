---
author: Toby C. Wilkinson
title: Fieldwalker.org projects
---

# Fieldwalker Projects database source

`fieldwalker / fieldwalker-projects`

A meta-database of archaeological surveys developed for The Fieldwalker.org.

Core data is held in the `/projects/` directory, most importantly the `_projects.csv` file.

## Submission of new projects to and updating pre-existing records in the database

To submit a single project not already in the database, go to the Fieldwalker.org
project pages, and fill out the relevant form.

https://www.fieldwalker.org/projects/add/

If you want to correct or update data in the database, you may contact
the editors at editors@fieldwalker.org or resubmit data using the "add" system
above.

Altenatively, if you are familiar with Git and the GitLab environment, you can
also make contributions and edits via the fork and merge-request process:-

- https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

## Re-use of this database during beta

While in beta, we ask that you do not re-use the data even though it is available openly online. The database will be officially launched with an open and relatively unrestrictive licence so that data and database structure can be re-used freely for research or educational purposes.
