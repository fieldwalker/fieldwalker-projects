% Encoding: UTF-8

@Article{az-Babaev2016ks,
  author  = {I. Babaev et al.},
  title   = {Der Karacamirli-Survey – Vorbericht zu den Geländearbeiten 2013-2014},
  journal = {Archäologische Mitteilungen aus Iran und Turan},
  year    = {2016},
  volume  = {48},
  pages   = {189-218},
}

@Book{cy-Given-etal2013li,
  title     = {Landscape and interaction: the Troodos Archaeological and Environmental Survey Project, Cyprus},
  publisher = {Council for British Research in the Levant},
  year      = {2013},
  editor    = {Given, M. and Knapp, A. B. and Noller, J. S. and Sollars, L. and Kassianidou, V.},
  series    = {Levant Supplementary Series},
  address   = {London},
}

@Book{cy-GivenKnapp2003sc,
  title     = {The Sydney Cyprus Survey Project: Social Approaches to Regional Archaeological Survey},
  publisher = {UCLA Institute of Archaeology},
  year      = {2003},
  author    = {Given, M. and A. B. Knapp},
  series    = {Monumenta Archaeologica 21},
  address   = {Los Angeles},
}

@Article{cy-Papantoniou2018re,
  author  = {Papantoniou, G. and Vionis, A.K.},
  title   = {The river as an economic asset: settlement and society in the Xeros Valley in Cyprus},
  journal = {Land},
  year    = {2018},
  volume  = {7.4},
  pages   = {157-186},
}

@InBook{cy-Trinks-etal2018tl,
  chapter   = {Archaeological prospection surveys},
  title     = {Two Late Cypriot City Quarters at Hala Sultan Tekke. The Söderberg Expedition 2010—2017},
  publisher = {Åström Editions},
  year      = {2018},
  author    = {Immo Trinks and Klaus Löcker and Peter M. Fischer},
  editor    = {P.M. Fischer and T. Bürge},
  series    = {Studies in Mediterranean Archaeology 147},
  address   = {Uppsala},
}

@PhdThesis{es-GarciaSanchez2012ap,
  author = {García Sánchez, J.},
  title  = {Arqueología y paisaje en el noroeste de Burgos: la transición de la Segunda Edad de Hierro a época romana a través del registro material},
  school = {Universidad de Cantabria. Departamento de Ciencias Históricas},
  year   = {2012},
  url    = {http://www.tdx.cat/handle/10803/80486},
}

@Article{es-Oller-etal2018ci,
  author  = {Oller, J. and Morera, J. and Olesti, O. and Mercadal, O.},
  title   = {Los ceretanos y la iberización del Pirineo oriental (s. iv-iii a. n. e.). Una nueva aproximación histórica y arqueológica},
  journal = {AESPA},
  year    = {2018},
  volume  = {91},
  pages   = {181-202},
}

@Article{fr-Revelles2019cl,
  author  = {J. Revelles and M. Ghilardi and V. Rossi and A. Currás and O. López-Bultó and G. Brkojewitsch and M. Vacchi},
  title   = {Coastal landscape evolution of Corsica island (W. Mediterranean): palaeoenvironments, vegetation history and human impacts since the early Neolithic period},
  journal = {Quaternary Science Reviews},
  year    = {2019},
  volume  = {225},
  pages   = {105993},
  doi     = {https://doi.org/10.1016/j.quascirev.2019.105993},
  url     = {https://www.sciencedirect.com/science/article/pii/S0277379119305517},
}

@Article{gr-Biagi-etal2016wn,
  author  = {P. Biagi and R. Nisbet and E. Starnini and N. Efstratiou and R. Michniak},
  title   = {Where Neanderthals and Mountains meet: The Middle Palaeolithic settlement of Samarina in Northern Pindus (Western Macedonia, Greece)},
  journal = {Eurasian Prehistory},
  year    = {2016},
  volume  = {13},
  number  = {1-2},
  pages   = {3-76},
}

@Article{gr-Bredaki-etal2009pf,
  author  = {Maria Bredaki and Fausto Longo and Mario Benzi},
  title   = {Progetto Festòs. Le ricerche della missione italo-greca: campagne 2007-2009},
  journal = {Annuario della Scuola Archeologica di Atene e delle Missioni Italiane in Oriente},
  year    = {2009},
  volume  = {87,2},
  pages   = {935-978},
}

@InCollection{gr-Carter-etal2017sn,
  author    = {Carter, T. and Contreras, D.A. and Holcomb, J. and Mihailović, D.D. and Skarpelis, N. and Campeau, K. and Moutsiou, T. and Athanasoulis, D.},
  title     = {The Stélida Naxos Archaeological Project: New studies of an early prehistoric chert quarry in the Cyclades},
  booktitle = {From Maple to Olive: Proceedings of a Colloquium to Celebrate the 40th Anniversary of the Canadian Institute in Greece, 10-11 June 2016},
  year      = {2017},
  editor    = {D.W. Rupp and J. Tomlinson},
  series    = {Publications of the Canadian Institute in Greece 10},
  pages     = {75-103},
  address   = {Athens},
}

@Book{gr-DavisBennet2017pr,
  title     = {The Pylos Regional Archaeological Project: A Retrospective},
  publisher = {American School of Classical Studies},
  year      = {2017},
  editor    = {Jack L. Davis and John Bennet},
}

@Article{gr-Gaignerot-Driessen2018pa,
  author  = {F. Gaignerot-Driessen and L. Fadin and R. Bardet and M. Devolder},
  title   = {La prospection de l’Anavlochos},
  journal = {Bulletin de correspondance hellénique},
  year    = {2018},
  volume  = {139-140.2},
  pages   = {951-974},
}

@InProceedings{gr-Gallimore-etal2017ta,
  author    = {S. Gallimore, S. James, W. Caraher, and D. Nakassis},
  title     = {To Argos: Archaeological Survey in the Western Argolid, 2014-2016},
  booktitle = {From Maple to Olive: Proceedings of a Colloquium to Celebrate the 40th Anniversary of the Canadian Institute in Greece, Athens, 10-11 June 2016},
  year      = {2017},
  editor    = {D.W. Rupp and J.E. Tomlinson},
  series    = {Publications of the Canadian Institute in Greece 10},
  pages     = {421-438},
  address   = {Athens},
  publisher = {Canadian Institute in Greece},
}

@Book{gr-Haggis2005as,
  title     = {The Archaeological Survey of the Kavousi Region, Kavousi I, The Results of the Excavations at Kavousi in Eastern Crete,},
  publisher = {INSTAP Academic Press},
  year      = {2005},
  author    = {D.C. Haggis},
  series    = {Prehistory Monographs 16},
  address   = {Philadelphia},
}

@Book{gr-Hayden2003-05rv,
  title     = {Reports on the Vrokastro Area, Eastern Crete},
  publisher = {University of Pennsylvania Museum of Archaeology and Anthropology and the Archaeological Museum},
  year      = {2003-2005},
  author    = {Hayden, B.},
  volume    = {1-3},
  address   = {Philadelphia and Herakleion},
}

@Article{gr-Knodell-etal2017ma,
  author   = {Knodell, A.R. and S. Fachard and K. Papangeli},
  title    = {The Mazi Archaeological Project 2016: Survey and Settlement Investigations in Northwest Attica (Greece).},
  journal  = {Antike Kunst},
  year     = {2017},
  volume   = {60},
  pages    = {146–163},
  keywords = {gr},
}

@Article{gr-Knodell-etal2020ia,
  author  = {Knodell, A.R. and D. Athanasoulis and Ž. Tankosić and J.F. Cherry and T. Garonis and E. Levine and D. Nenova, and H. Öztürk},
  title   = {An Island Archaeology of Uninhabited Landscapes: Offshore Islets Near Paros, Greece (The Small Cycladic Islands Project).},
  journal = {Journal of Island and Coastal Archaeology},
  year    = {2020},
  doi     = {10.1080/15564894.2020.1807426},
}

@PhdThesis{gr-Moody1987ec,
  author = {Moody, J. A.},
  title  = {The environmental and cultural prehistory of the Khania region of West Crete: Neolithic through Late Minoan III},
  school = {University of Minnesota},
  year   = {1987},
}

@Article{gr-Murray-etal2020be,
  author  = {S. Murray and C. Pratt and R. Stephan and M. McHugh and G. Erny and A. Psoma and B. Lis and P. Sapirstein},
  title   = {The 2019 Bays of East Attica Regional Survey (BEARS): New Evidence for the Archaeology of the Bay of Porto Raphti},
  journal = {Mouseion},
  year    = {2020},
  volume  = {17.2},
  pages   = {323-394},
  doi     = {10.3138/mous.17.2.005},
}

@Book{gr-RenfrewWagstaff1982ip,
  title     = {An Island Polity: The Archaeology of Exploitation on Melos},
  publisher = {Cambridge University Press},
  year      = {1982},
  editor    = {C. Renfrew and M. Wagstaff},
  address   = {Cambridge},
  keywords  = {gr},
}

@Article{gr-Tartaron-etal2011sh,
  author  = {T. F. Tartaron and D. J. Pullen and R. K. Dunn and L. Tzortzopoulou-Gregory and A. Dill},
  title   = {The Saronic Harbors Archaeological Research Project: Investigations at Mycenaean Kalamianos, 2007–2009},
  journal = {Hesperia},
  year    = {2011},
  volume  = {80},
  pages   = {559–634},
}

@Article{gr-VanWijngaarden2013na,
  author  = {Van Wijngaarden, G. J.M. and G. Kourtesi-Phillipakis and N. Pieters},
  title   = {New Archaeological sites and finds on Zakynthos},
  journal = {Pharos. Journal of the Netherlands Institute at Athens},
  year    = {2013},
  volume  = {19.1},
  pages   = {127-159},
}

@InCollection{hr-Lipovc-etal2017fa,
  author    = {Lipovc Vrkljan, G. and Konestra, A. and Šegvić, N.},
  title     = {Felix Arba – reconstructing urban and rural economic capacities through GIS},
  booktitle = {Mapping urban changes},
  year      = {2017},
  editor    = {A. Plosnić Škarić},
  pages     = {314-335},
  address   = {Zagreb},
}

@Book{il-Frankel-etal2001sd,
  title     = {Settlement Dynamics and Regional Diversity in Ancient Upper Galilee: Archaeological Survey of Upper Galilee},
  publisher = {Israel Antiquities Authority},
  year      = {2001},
  author    = {Rafael Frankel and Nimrod Gezov and Mordechai Aviam and Avi Degani},
  series    = {Israel Antiquities Authority Reports 14},
  address   = {Jerusalem},
}

@Book{il-Lehmann2012ah,
  title     = {Archaeological Survey of Israel. Map of Ahihud (20)},
  publisher = {Israel Antiquities Authority},
  year      = {2012},
  author    = {Lehmann, G. and Peilstöcker, M.},
  address   = {Jerusalem},
}

@Article{il-Maeir2017ts,
  author  = {Maeir, Aren M.},
  title   = {The Tell eṣ-Ṣâfi/Gath Archaeological Project: Overview},
  journal = {Near Eastern Archaeology},
  year    = {2017},
  volume  = {80/4},
  pages   = {212–231},
}

@Book{il-Weiss2004as,
  title     = {Archaeological Survey of Israel, Map of Nes Harim (104).},
  publisher = {Israel Antiquities Authority},
  year      = {2004},
  author    = {Weiss D. and Zissu B. and Solimany G.},
  address   = {Jerusalem},
}

@Book{il-ZertalBar2019mh,
  title     = {The Manasseh Hill Country Survey Volume 5: The Middle Jordan Valley, from Wadi Fasael to Wadi 'Aujah},
  publisher = {Brill},
  year      = {2019},
  author    = {Zertal, A. and Bar, S.},
  address   = {Leiden and Boston},
}

@Article{iq-Ur-etal2013ac,
  author  = {Ur, J. A., L. De Jong, J. Giraud, J. F. Osborne, and J. Macginnis},
  title   = {Ancient Cities and Landscapes in the Kurdistan Region of Iraq: The Erbil Plain Archaeological Survey 2012 Season},
  journal = {Iraq},
  year    = {2013},
  volume  = {75},
  pages   = {89-117},
}

@Article{iq-Ur-etal2021ep,
  author  = {Ur, J. A. and N. Babakr and R. Palermo and M. Soroush and S. Ramand and K. Nováček},
  title   = {The Erbil Plain Archaeological Survey: Preliminary Results, 2012-2018},
  journal = {Iraq},
  year    = {2021},
  doi     = {10.1017/irq.2021.2},
}

@Book{it-Attema-etal2011bs,
  title     = {Between Satricum and Antium. Settlement dynamics in a coastal landscape in Latium},
  publisher = {Peeters},
  year      = {2011},
  author    = {P. Attema and T. De Haas and G. Tol 2011},
  series    = {Babesch supplement 18},
  address   = {Leuven},
}

@Article{it-Biagi1998pr,
  author  = {Biagi P.},
  title   = {Prospezioni e ricerchesu uno spartiacque delle Alpi meridionali: gli effetti dell'impatto antropico fra l'inizio dell'Olocene e il Medio Evo},
  journal = {Saguntum},
  year    = {1998},
  volume  = {31},
  pages   = {117-124},
}

@Article{it-Ceraudo2020ct,
  author  = {G. Ceraudo},
  title   = {Considerazioni topografiche a margine della scoperta del cosiddetto Cesare di Aquinum: la fortuna è nel metodo},
  journal = {Rendiconti della Pontificia Accademia di Archeologia},
  year    = {2020},
  volume  = {91},
  pages   = {249-274},
}

@Article{it-Dommelen2014av,
  author  = {P. van Dommelen and C. Gómez Bellard},
  title   = {Anfore, vino e l'orto: per un'archeologia dei paesaggi rurali ed agrari nel mondo punico},
  journal = {Rivista di Studi Fenici},
  year    = {2014},
  volume  = {40.2-2012},
  pages   = {251-66},
}

@Article{it-Murphy-etal2019la,
  author   = {Murphy, Elizabeth A., Thomas P. Leppard, Andrea Roppa, Emanuele Madrigali, and Carmen Esposito},
  title    = {The Landscape Archaeology of Southwest Sardinia project: New data and method from the insular Mediterranean},
  journal  = {Journal of Field Archaeology},
  year     = {2019},
  volume   = {4(6)},
  pages    = {367-382},
  keywords = {it},
}

@Article{it-Plekhov-etal2020as,
  author  = {Plekhov, D. and Gosner, L. and Smith, A. and Nowlin, J.},
  title   = {Applications of Satellite Remote Sensing for Archaeological Survey: A Case Study from the Sinis Archaeological Project, Sardinia},
  journal = {Advances in Archaeological Practice},
  year    = {2020},
  volume  = {1-14},
  doi     = {10.1017/aap.2020.1},
}

@Article{it-Severa-etal2020lt,
  author  = {Christopher Sevara and Roderick B. Salisbury and Michael Doneus and Erich Draganits and Ralf Totschnig and Cipriano Frazzetta and Sebastiano Tusa},
  title   = {A Landscape in Transitions: Guletta, a Multiperiod Settlement along the Mazaro River in Western Sicily},
  journal = {Journal of Field Archaeology},
  year    = {2020},
  volume  = {45},
  number  = {5},
  pages   = {334-354},
  doi     = {10.1080/00934690.2020.1734898},
}

@Article{jo-Kerner2019nr,
  author  = {Susanne Kerner},
  title   = {New Research into the Early and Middle Bronze Age in Central Jordan: Murayghat},
  journal = {Journal of Mediterranean Archaeology and Heritage Studies},
  year    = {2019},
  volume  = {7.2},
  pages   = {165-186},
}

@Article{jo-Knodell-etal2017bu,
  author  = {A.R. Knodell and S.E. Alcock and C. Tuttle and C.F. Cloke and T. Erickson-Gini and C. Feldman and G.O. Rollefson and M. Sinibaldi and C. Vella},
  title   = {The Brown University Petra Archaeological Project: Landscape Archaeology in the Northern Hinterland of Petra, Jordan.},
  journal = {American Journal of Archaeology},
  year    = {2017},
  volume  = {121(4)},
  pages   = {621–683},
}

@Article{om-DuringOlijdam2015rs,
  author  = {During B.S. and Olijdam E.},
  title   = {Revisiting the Suhar Hinterlands: The Wadi al-Jizi Archaeological Project},
  journal = {Proceedings of the seminar for Arabian Studies},
  year    = {2015},
  volume  = {45},
  pages   = {93-106},
}

@Article{sy-Dornemann2003ss,
  author  = {Dornemann, Rudolph H.},
  title   = {Seven Seasons of ASOR Excavations at Tell Qarqur, Syria, 1993-1999},
  journal = {Annual of the American Schools of Oriental Research},
  year    = {2003},
  volume  = {56},
  pages   = {1–141},
}

@InBook{sy-PhilipBradbury2016fr,
  chapter   = {Settlement in the Upper Orontes Valley from the Neolithic to the Islamic Period: an instance of punctuated equilibrium},
  pages     = {377-395},
  title     = {Le fleuve rebelle. Géographie historique du moyen Oronte d’Ebla à l’époque médiévale.},
  publisher = {Presses de l’Ifpo},
  year      = {2016},
  author    = {Philip, G. and J. Bradbury},
  editor    = {D. Parayre},
  address   = {Beyrouth},
}

@InBook{tn-Mosca2012nd,
  chapter   = {Nuovi dati sulla topografia dell’area La Malga e osservazioni sul rifornimento idrico a Cartagine},
  pages     = {427-440},
  title     = {L’Africa romana: trasformazione dei paesaggi del potere nell’Africa settentrionale fino alla fine del mondo antico Atti del XIX convegno di studio Sassari, 16-19 dicembre 2010},
  publisher = {Carocci},
  year      = {2012},
  author    = {Mosca, A.},
  editor    = {Cocco, M. B. and Gavini, A. and Ibba, A.},
  address   = {Roma},
}

@InProceedings{tr-Elton2008ga,
  author    = {Elton, H.},
  title     = {Göksu Archaeological Project 2005-2006},
  booktitle = {Araştırma Sonuçları Toplantısı},
  year      = {2008},
  number    = {25.2},
  pages     = {237-250},
  address   = {Ankara},
  publisher = {Ministry of Culture and Tourism},
}

@InCollection{tr-Erdogu2008sp,
  author    = {Erdoğu, B and Özbaşaran, M.},
  title     = {Salt in Prehistoric Central Anatolia},
  booktitle = {Sel, eau et forêt. Hier et aujourd'hui},
  publisher = {Universite de Franche-Comte},
  year      = {2008},
  editor    = {O. Weller and A. Dufraisse and P. Petrequin},
  pages     = {163-174},
  address   = {Besancon},
}

@Article{tr-Massa-etal2020lo,
  author  = {Massa M. and Bachhuber C. and Şahin F. and Erpehlivan H. and Osborne J. and A.J. Lauricella},
  title   = {A landscape-oriented approach to urbanisation and early state formation on the Konya and Karaman plains, Turkey},
  journal = {Anatolian Studies},
  year    = {2020},
  volume  = {70},
  pages   = {45-75},
}

@Article{tr-Pirson2018bu,
  author  = {F. Pirson},
  title   = {Pergamon – Bericht über die Arbeiten in der Kampagne 2017},
  journal = {Archäologische Anzeigher},
  year    = {2018},
  volume  = {2018/2},
  pages   = {109–192},
  note    = {150-167 (S. Feuser - E. Laufer)},
}

@InBook{tr-Pirson-etal2015us,
  chapter = {Elaia: Eine aiolische Polis im Dienste der hellenistischen Residenzstadt Pergamon?},
  pages   = {22–55},
  title   = {Urbane Strukturen und bürgerliche Identität im Hellenismus. Die hellenistische Polis als Lebensform 5},
  year    = {2015},
  author  = {F. Pirson and G. Ateş and M. Bartz and H. Brückner and S. Feuser and U. Mania and L. Meier and M. Seeliger},
  editor  = {A. Matthaei and M. Zimmermann},
  address = {Heidelberg},
}

@Book{tr-Scachner2019ak,
  title     = {Assyriens Könige an einer der Quellen des Tigris, Archäologische Forschungen im Höhlensystem von Bırkleyn und am so genannten Tigris-Tunnel},
  publisher = {Wasmuth},
  year      = {2019},
  author    = {Andreas Schachner},
  series    = {Istanbuler Forschungen 51},
  address   = {Tübingen},
}

@InCollection{tr-SerifogluKucukbezci2019tk,
  author    = {T.E. Şerifoğlu and H.G. Küçükbezci},
  title     = {Taşeli-Karaman Archaeological Project: The First Two Seasons},
  booktitle = {The Archaeology of Anatolia: Recent Discoveries (2017- 2018)},
  publisher = {Cambridge Scholars Publishing},
  year      = {2019},
  editor    = {S. Steadman and G. McMahon},
  volume    = {3},
  pages     = {175-192},
  address   = {Newcastle Upon Tyne},
}

@Article{tr-Vos-etal2007vz,
  author  = {de Vos M., Hoffmann A., Attoui R., Andreoli M., Polla S.},
  title   = {Vorbericht zu den in den Jahren 2003 bis 2005 auf dem Berg Karasis (bei Kozan/Adana) und in seiner Umgebung durchgeführten Untersuchungen.},
  journal = {Istanbuler Mitteilungen},
  year    = {2007},
  volume  = {57},
  pages   = {447-459},
}

@Article{tr-WilkinsonSlawisch2017pt,
  author   = {T. C. Wilkinson and A. Slawisch},
  title    = {Panormos 2017: Intensive Survey on the Milesian Peninsula},
  journal  = {Heritage Turkey},
  year     = {2017},
  volume   = {7},
  pages    = {32–33},
  keywords = {tr},
}

@Comment{jabref-meta: databaseType:bibtex;}

@Comment{jabref-meta: saveOrderConfig:specified;bibtexkey;false;year;false;abstract;false;}
